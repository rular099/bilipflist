# BiliPFList

b站集市 200 元以上手办列表。方便按照价位查找手办、按照名字搜索手办。

每 2 小时更新。

### 用法

1. 打开 pflist.csv 文件，查找手办代码。注意，gitlab 表格形式无法显示完整文档，请使用“显示源”或者“打开原始文件”进行查看。
![示意图1](./images/1735892700081.jpg)

2. 使用查找到的手办代码，替换下面链接对应位置：

https://mall.bilibili.com/neul-next/index.html?page=magic-market_detail&itemsId={手办代码}

例如上图中红色方框中的手办代码，链接为：

https://mall.bilibili.com/neul-next/index.html?page=magic-market_detail&itemsId=138639206810

3. 访问 2 中的链接购买。

**注**：pflist.csv 文件中包含 5 列数据，第1列是手办代码，第2列是手办名称，第3列是售价，第4列是市场价，第5列是售价对比市场价的折扣。默认按照售价排序。如果希望按照市场价或者折扣排序，可以将 pflist.csv 下载到本地，用 Excel 等工具打开进行操作。
